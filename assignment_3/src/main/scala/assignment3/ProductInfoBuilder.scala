package assignment3

case class ProductResult(productID: Int,
                         productName: String,
                         productBrand: String,
                         productDescription: String,
                         productPrice: Float,
                         productInventory: Int)

class ProductInfoBuilder() {
  private var id: Option[Int]  = None
  private var productName: Option[String] = None
  private var productBrand: Option[String] = None
  private var productDescription: Option[String] = None

  private var productPrice: Option[Float] = None
  private var productInventory: Option[Int] = None

  def addMainInfo(id: Int, name: String,
                   brand: String, description: String): Unit = {
    this.id = Option(id)
    this.productName = Option(name)
    this.productBrand = Option(brand)
    this.productDescription = Option(description)
  }

  def addProductPrice(price: Float): Unit =
    this.productPrice = Option(price)

  def addProductInventory(inventory: Int): Unit =
    this.productInventory = Option(inventory)

  def timeout() : Unit = {
    if (id.isEmpty) {
      id = Some(-1)
      productName = Some("No product found")
      productBrand = Some("TimeOut happened")
      productDescription = Some("TimeOut happened")
    }
    if (productPrice.isEmpty) productPrice = Some(0)
    if (productInventory.isEmpty) productInventory = Some(0)
  }


  def isComplete() : Boolean =
    id.isDefined &&
      productName.isDefined &&
      productBrand.isDefined &&
      productDescription.isDefined &&
      productPrice.isDefined &&
      productInventory.isDefined

  def result() : ProductResult =
    ProductResult(id.get,
      productName.get,
      productBrand.get,
      productDescription.get,
      productPrice.get,
      productInventory.get)
}

