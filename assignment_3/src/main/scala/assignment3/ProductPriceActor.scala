package assignment3

import akka.actor.SupervisorStrategy.Restart
import akka.actor.{Actor, ActorLogging}

case class ProductPrice(productID: Int, price: Int)

class ProductPriceActor extends Actor with ActorLogging{

  var products: Map[Int, ProductPrice] = Map((1, ProductPrice(1, 700)),
    (2,ProductPrice(1, 800)),
    (3,ProductPrice(1, 1000)))

  def getProd(id: Int) : ProductPrice = {
    val product: Option[ProductPrice] = products.get(id)
    product.get
  }

  def receive: Receive = {
    case ForceRestart => Restart

    case GetProductPrice(productID, replyTo) =>
      val product = getProd(productID)
      replyTo ! product
      log.info("Product price sent!")

  }
}
