package assignment3

import akka.actor.SupervisorStrategy.Restart
import akka.actor.{Actor, ActorLogging}
case class HasBeenAddedToFavorite(productID: Int)

class FavoriteProductActor extends Actor with ActorLogging{

  var userToFavorite: Map[Int, Int] = Map()

  def getProds(userID: Int) : Int = {
    userToFavorite.get(userID).get
  }

  def setProds(userID: Int, prodID: Int) ={
    userToFavorite = userToFavorite.-(userID)
    userToFavorite = userToFavorite.+((userID,prodID))
  }

  def receive: Receive = {
    case ForceRestart => Restart

    case SaveToFavorites(userID, productID, replyTo) =>
//      setProds(userID, productID)
      replyTo ! HasBeenAddedToFavorite(productID)
      (log.info("Has been added"))
  }

}
