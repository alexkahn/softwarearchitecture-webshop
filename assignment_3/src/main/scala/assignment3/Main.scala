
package assignment3

import akka.actor.{ActorSystem, Props}


//#main-class
object Main extends App {


   // actor system
  val system: ActorSystem = ActorSystem("OnlineShop")

  val productinfo = system.actorOf(Props[ProductInfoActor])
  val productprice = system.actorOf(Props[ProductPriceActor])
  val productinventory = system.actorOf(Props[ProductInventoryActor])
  val favorite = system.actorOf(Props[FavoriteProductActor])

  val superVisorActor = system.actorOf(Props[SuperVisorActor])

  val gateway = system.actorOf(Props(new GatewayActor(productinfo, productprice, productinventory, favorite, superVisorActor)), "gateway")
  val client = system.actorOf(Props( new ClientActor(gateway)), "client")



}
//#main-class

