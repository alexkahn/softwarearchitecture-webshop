package assignment3

import akka.actor.{Actor, ActorLogging}

case class ProductInfo(productID: Int, productName: String, productBrand: String, productDescription: String)


class ProductInfoActor extends Actor with ActorLogging {

  var products: Map[Int, ProductInfo] = Map((1, ProductInfo(1, "iPhone 7", "Apple", "This is the 8th generation iPhone")),
    (2,ProductInfo(2, "iPhone 8", "Apple", "This is the 9th generation iPhone")),
    (3,ProductInfo(3, "iPhone X", "Apple", "This is the 10th generation iPhone")))

  def getProd(id: Int) : ProductInfo = {
    val product: Option[ProductInfo] = products.get(id)
    product.get
  }

  def receive: Receive = {
    case GetProductInfo(productID, replyTo) =>
      val product = getProd(productID)
      replyTo ! product
      log.info("Product info sent!")
  }

}
