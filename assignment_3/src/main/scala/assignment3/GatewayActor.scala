package assignment3

import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, Props}


case class GetProductInfo(productID: Int, replyTo: ActorRef)
case class GetProductPrice(productID: Int, replyTo: ActorRef)
case class GetProductInventory(productID: Int, replyTo: ActorRef)
case class GetTeaserProductInfo(productID: Int, replyTo: ActorRef)
case class SaveToFavorites(userID: Int, productID: Int, replyTo: ActorRef)

class GatewayActor(productInfoActor: ActorRef,productPriceActor: ActorRef, productInventoryActor: ActorRef, favoriteProductActor: ActorRef, superVisorActor: ActorRef ) extends Actor with ActorLogging {


  def receive: Receive = {

    case GetFullDetails(productID, replyTo) =>
      val aggregator = context.actorOf(Props(new ProductAggregator(replyTo, superVisorActor)))
      productInfoActor ! GetProductInfo(productID, aggregator)
      productPriceActor ! GetProductPrice(productID, aggregator)
      productInventoryActor ! GetProductInventory(productID, aggregator)
      log.info("Get full details sent!")

    case GetTeaserDetails(productID, replyTo) => // Contact the individual microservice(s)
      val corrID = UUID.randomUUID()
      val childActor = context.actorOf(Props(new ResponseHandlerChild(corrID, replyTo, superVisorActor, productInfoActor)))
      productInfoActor ! GetProductInfo(productID, childActor)
      log.info("Get the teaser details sent")

    case SaveFavorite(userID, productID, replyTo) =>
      val corrID = UUID.randomUUID()
      val childActor = context.actorOf(Props(new ResponseHandlerChild(corrID, replyTo, superVisorActor, productInfoActor)))
      favoriteProductActor ! SaveToFavorites(userID, productID, childActor)
      log.info("Sent the message to save " + productID + "for the user with id " + userID)


  }
}
