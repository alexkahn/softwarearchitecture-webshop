package assignment3

import java.util.UUID

import akka.actor.{Actor, ActorLogging, ActorRef, ReceiveTimeout}

import scala.concurrent.duration._

case class TimeOutOccured(id: UUID)
case class ForceRestart()
case class Teaser(productID: Int, productName: String, productBrand: String, productDescription: String)
case class RestartService(microservice: ActorRef)

class ResponseHandlerChild(corrID: UUID, replyTo: ActorRef,supervisor: ActorRef , micorservice: ActorRef) extends Actor with ActorLogging {

  context.setReceiveTimeout(10 seconds)
  override def postStop = {
    log.info("shutting down response handler child for {}", corrID)
  }

   def receive: Receive = {

     case ReceiveTimeout =>
       log.warning("verification initiation timed out for {}", corrID)
       replyTo ! TimeOutOccured(corrID)
       supervisor ! RestartService(micorservice)
       context.stop(self)

     case ProductInfo(productID,
                      productName,
                      productBrand,
                      productDescription) =>
       replyTo ! Teaser(productID, productName,productBrand, productDescription)
       log.info("Product info went trough ResponseHandler")
       context.stop(self)

     case HasBeenAddedToFavorite(productID) =>
       replyTo ! HasBeenAddedToFavorite(productID)
       log.info("Product adding went trough ResponseHandler")
       context.stop(self)

     case msg =>
       log.error("Weird message " + msg)
   }

}
