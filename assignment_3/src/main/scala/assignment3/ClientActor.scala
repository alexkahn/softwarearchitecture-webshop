package assignment3

import akka.actor.{Actor, ActorLogging, ActorRef}


private case object Start
private case object AskGetFullDetails
private case object AskGetTeaser
private case object AskSaveFavorite

case class GetFullDetails(productID: Int, replyTo: ActorRef)
case class GetTeaserDetails(productID: Int, replyTo: ActorRef)
case class SaveFavorite(userID: Int, productID: Int, replyTo: ActorRef)
case class RestartServices()

class ClientActor(gateway: ActorRef) extends Actor with ActorLogging {


//
//  gateway ! GetFullDetails(1, self)
  log.info("Full details asked!")
//  gateway ! GetTeaserDetails(15, self)
  log.info("Teaser details asked")
//  gateway ! SaveFavorite(1,1, self)
  gateway ! GetFullDetails(15, self)

    def receive: Receive = {

      // Asking information
      case AskGetFullDetails => gateway ! GetFullDetails(1, self)
        log.info("Full details asked!")
      case AskGetTeaser => gateway ! GetTeaserDetails(1, self)
        log.info("Teaser details asked")
      case AskSaveFavorite ⇒ gateway ! SaveFavorite(1,1, self)
        log.info("Asked to save a favorite")
//      case msg => log.info("Received message: {}", msg)

      // Receiving information

      // Full details
      case msg: ProductResult =>
        if (msg.productID > 1) {
          log.info("Received the full product info: " + msg)
        }
        else log.info("Timeout received")


      // Teaser
      case msg: Teaser =>
        log.info("Received the teaser info: " + msg)
//        context.system.terminate()

      // Favorite worked

      case msg: HasBeenAddedToFavorite =>
        log.info("Received the has been added info: " + msg)
//        context.system.terminate()

      case msg: TimeOutOccured =>
        log.info("Timeout received")

    }
}