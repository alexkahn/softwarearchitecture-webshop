package assignment3

import akka.actor.{Actor, ActorLogging, ActorRef, ReceiveTimeout}

import scala.concurrent.duration._

case class RestartAllServices()

class ProductAggregator(replyTo: ActorRef, supervisor: ActorRef) extends Actor with ActorLogging{

  val builder = new ProductInfoBuilder()
  context.setReceiveTimeout(1 seconds)


  def receiveResult : Receive = {
    case ProductInfo(productID, productName, productBrand, productDescription) =>
      builder.addMainInfo(productID,
                          productName,
                          productBrand,
                          productDescription)
    case ProductPrice(productID, price) => builder.addProductPrice(price)

    case ProductInventory(productID, inventory) => builder.addProductInventory(inventory)

    case ReceiveTimeout =>
      log.warning("Builder is using defaults for timed out result.")
      replyTo ! builder.timeout()
      supervisor ! RestartAllServices()
      context.stop(self)
  }

  def checkComplete : Receive = {
    case msg =>
      log.debug("Aggregator received result: {}", msg)
      if (builder.isComplete()) {
        log.debug("Aggregated result is now complete")
        replyTo ! builder.result
        context.stop(self)
      }
  }
  def receive : Receive = receiveResult andThen checkComplete
}
