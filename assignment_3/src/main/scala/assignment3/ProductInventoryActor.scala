package assignment3

import akka.actor.SupervisorStrategy.Restart
import akka.actor.{Actor, ActorLogging}

case class ProductInventory(i: Int, inventory: Int)



class ProductInventoryActor extends Actor with ActorLogging{

  var products: Map[Int, ProductInventory] = Map((1, ProductInventory(1, 10)),
     (2,ProductInventory(1, 43)),
    (3,ProductInventory(1, 50)))

  def getProd(id: Int) : ProductInventory = {
    val product: Option[ProductInventory] = products.get(id)
    product.get
  }
  def receive: Receive = {

    case ForceRestart => Restart

    case GetProductInventory(productID, replyTo) =>
      val product = getProd(productID)
      replyTo ! product
      log.info("Product price sent!")

  }
}
