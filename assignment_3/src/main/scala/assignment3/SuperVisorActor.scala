package assignment3

import akka.actor.{Actor, ActorLogging, ActorRef}

class SuperVisorActor  extends Actor with ActorLogging {

  def receive: Receive = {

    case RestartService(microservice: ActorRef) =>
      microservice ! ForceRestart
  }

}
